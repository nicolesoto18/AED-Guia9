#include <iostream>
using namespace std;

typedef struct _nodo{ // Estructura del nodo
	int numero;
	struct _nodo *sig;
} nodo;


class Encadenamiento{
    
  public:
        Encadenamiento();
        void copiarArreglo(nodo **arreglo1, int *arreglo, int tamano);
        void imprimirLista(nodo **arreglo, int posicion);
        void metodoEncadenamiento(int *arreglo, int tamano, int num, int posicion);
        
};        
