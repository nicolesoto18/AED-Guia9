#include <iostream>
#include "Encadenamiento.h"

using namespace std;

Encadenamiento::Encadenamiento(){

}

void Encadenamiento::copiarArreglo(nodo **arreglo1, int *arreglo, int tamano){
    for(int i = 0; i < tamano; i++){
	    arreglo1[i]->numero = arreglo[i];
        arreglo1[i]->sig = NULL;
    }
}


void Encadenamiento::imprimirLista(nodo **arreglo, int posicion){
    nodo *aux = NULL;
    aux = arreglo[posicion];

	while(aux != NULL){
        if(aux->numero != 0){
		    cout << "[" << aux->numero << "] - " << endl;
        }

		aux = aux->sig;
    }
}


void Encadenamiento::metodoEncadenamiento(int *arreglo, int tamano, int num, int posicion){
    nodo *temporal = NULL;
    nodo *arregloAux[tamano];

    for(int i = 0; i < tamano; i++){
        arregloAux[i] = new nodo();
		arregloAux[i]->sig = NULL;
		arregloAux[i]->numero = -1;
    }
	
    copiarArreglo(arregloAux, arreglo, tamano);

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){
        cout << num << " esta en la posicion " << posicion << endl;
    }

    else{
	    temporal = arregloAux[posicion]->sig;

        while(temporal != NULL && temporal->numero != num){
            temporal = temporal->sig;
        }
    
        if(temporal == NULL){
            cout << num << " no se encuentra en la lista" << endl;
            cout << " Lista "  << endl;
	        imprimirLista(arregloAux, posicion);
        }

		else{
    		cout << num << " se encuentra en la posicion " << posicion << endl;
            cout << "\nLista "  << endl;
	        imprimirLista(arregloAux, posicion);
        }
    }
    
}


