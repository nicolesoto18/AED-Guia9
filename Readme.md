				    
                                                        	   Métodos de búsqueda
							  
+ Empezando
    El programa consiste en la implementación de cuatro métodos de búsqueda, prueba líneal, prueba cuadrática, doble dirección hash y encadenamiento.
Para determinar que método se utilizara debe ejecutar el programa e indicar la letra inicial del método elegido (L, C, D o E), luego se le preguntara el tamaño del arreglo, una vez ingresado vera un menú con tres opciones, la primera le permite ingresar números, cuando ingrese un número se mostrara el arreglo con los elementos que contiene, si hay colisión se indicara en que posición fue y hacia donde se movio el dato, con la segunda opción puede buscar un número, se le indicara en que posición esta o si este no esta en el arreglo y finalmente la tercera opción permite cerrar el programa.

+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
		[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación.
	
    Para compilar el programa se utiliza el comando:
		[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa parámetro
			parámetro --> Es el parámetro donde se indica el método para resolver las colisiones.
		       parámetro puede ser:
		       L --> Prueba líneal.
		       C --> Prueba cuadrática.
		       D --> Doble dirección hash.
		       E --> Encadenamiento.

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.

