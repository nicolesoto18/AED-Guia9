#include <iostream>
#include "ColisionesIngreso.h"

using namespace std;

ColisionesIngreso::ColisionesIngreso(){

}

void ColisionesIngreso::imprimirColision(int *arreglo, int tamano){
    for(int i = 0; i < tamano; i++){
        cout << i  << " [" << arreglo[i] << "]"<< endl;
    }
}

void ColisionesIngreso::colisionesPruebaLineal(int posicion, int *arreglo, int tamano, int num){
    int nuevaPosicion;
    int contador;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ // Revisa si el número ya esta en el arreglo
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        contador = 0;
        nuevaPosicion = posicion + 1; // Avanza una posición

        while(arreglo[nuevaPosicion] != -1 && nuevaPosicion <= tamano && nuevaPosicion != posicion && arreglo[nuevaPosicion] != num){
            nuevaPosicion = nuevaPosicion + 1;
            if(nuevaPosicion == tamano + 1){
                nuevaPosicion = 0; // Vuelve al inicio
            }
            contador = contador + 1; // Contador para no pasar el tamaño del arreglo
        }

        if(contador == tamano){ // Revisa si el contador alcanzo el tamaño del arreglo
            cout << "No quedan espacios en el arreglo" << endl;
        }

        if(arreglo[nuevaPosicion] == -1){ // Indica en que posición queda el número despues de moverse
            arreglo[nuevaPosicion] = num;
            cout << num << " se movio a la posición " << nuevaPosicion << "\n" << endl;
            imprimirColision(arreglo, tamano);
        }
    }
}
 

void ColisionesIngreso::colisionesPruebaCuadratica(int posicion, int *arreglo, int tamano, int num){
    int i; // Variable que aumentara cuadráticamente
    int nuevaPosicion;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ // Revisa si el número ya esta en el arreglo
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        i = 1;
        nuevaPosicion = (posicion + (i*i)); // Avanza a la nueva posición con un incremente al cuadrado
        while(arreglo[nuevaPosicion] != -1 && arreglo[nuevaPosicion] != num){ // Revisa si la nueva posición esta vacia
            i = i + 1; //Aumenta en uno para avanzar una posición
            nuevaPosicion = (posicion + (i*i)); // Genera una nueva posición

            if(nuevaPosicion > tamano){ 
                i = 1;
                nuevaPosicion = 0; // Vuelve al inicio del arreglo
                posicion = 0;
            }
        }
        
        if(arreglo[nuevaPosicion] == -1){ // Indica en que posición queda el número despues de moverse
            arreglo[nuevaPosicion] = num;
            cout << num << " se movio a la posición " << nuevaPosicion << "\n" << endl;
            imprimirColision(arreglo, tamano);
        }
    }
}


void ColisionesIngreso::colisionesDobleDireccion(int posicion, int *arreglo, int tamano, int num){ //REVISAR
    int nuevaPosicion;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ // Revisa si el número ya esta en el arreglo
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        nuevaPosicion = ((posicion + 1)%tamano - 1) + 1; // Se resta uno al tamaño porque el arreglo parte desde cero
  
        while(nuevaPosicion <= tamano && arreglo[nuevaPosicion] != -1 && nuevaPosicion != posicion && arreglo[nuevaPosicion] != num){
            nuevaPosicion = ((nuevaPosicion + 1)%tamano - 1) + 1;
        }
        
        if(arreglo[nuevaPosicion] == -1){ // Revisa si la nueva posición esta vacia
            arreglo[nuevaPosicion] = num;
            if(nuevaPosicion == tamano + 1){
                cout << "No quedan espacios en el arreglo" << endl;
            }

            else{
                cout << num << " se movio a la posición " << nuevaPosicion << "\n" << endl;
                imprimirColision(arreglo, tamano);
            }
        }
    }
}



