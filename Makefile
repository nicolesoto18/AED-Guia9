prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = programa.cpp ColisionesIngreso.cpp ColisionesBusqueda.cpp Encadenamiento.cpp
OBJ = programa.o ColisionesIngreso.o ColisionesBusqueda.o Encadenamiento.o
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

