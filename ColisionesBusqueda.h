#include <iostream>

using namespace std;

#ifndef COLISIONES_BUSQUEDA_H
#define COLISIONES_BUSQUEDA_H

class ColisionesBusqueda{

    private:

    public:
        ColisionesBusqueda(); // Constructor

        void imprimirColision(int *arreglo, int tamano);
        void colisionesPruebaLineal(int posicion, int *arreglo, int tamano, int num);
        void colisionesPruebaCuadratica(int posicion, int *arreglo, int tamano, int num);
        void colisionesDobleDireccion(int posicion, int *arreglo, int tamano, int num);
};
#endif
