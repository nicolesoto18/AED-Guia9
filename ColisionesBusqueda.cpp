#include <iostream>
#include "ColisionesBusqueda.h" // Definición de la clase

using namespace std;

ColisionesBusqueda::ColisionesBusqueda(){

}

void ColisionesBusqueda::imprimirColision(int *arreglo, int tamano){
    for(int i = 0; i < tamano; i++){
        cout << i  << " [" << arreglo[i] << "]"<< endl;
    }
}


void ColisionesBusqueda::colisionesPruebaLineal(int posicion, int *arreglo, int tamano, int num){
    int nuevaPosicion;
    
    if(arreglo[posicion] != -1 && arreglo[posicion] == num){
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

	else{
    	nuevaPosicion = posicion + 1;
        while(arreglo[nuevaPosicion] != -1 && nuevaPosicion <= tamano && nuevaPosicion != posicion && arreglo[nuevaPosicion] != num){
            nuevaPosicion = nuevaPosicion + 1;
            if(nuevaPosicion == tamano + 1){
                nuevaPosicion = 0; // Vuelve al inicio
            }
        }
    
        if(arreglo[nuevaPosicion] == -1 || (nuevaPosicion == posicion)){
            cout << num << " no se encuentra en el arreglo" << endl;
            imprimirColision(arreglo, tamano);
        }

	    else{
            cout << num << " se encuentra en la posición " << nuevaPosicion << endl;
            imprimirColision(arreglo, tamano);
        }
    }
}


void ColisionesBusqueda::colisionesPruebaCuadratica(int posicion, int *arreglo, int tamano, int num){
    int i;
    int nuevaPosicion;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ // Revisa si el número ya esta en el arreglo
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        i = 1;
        nuevaPosicion = (posicion + (i*i)); // Avanza a la nueva posición con un incremente al cuadrado
        while(arreglo[nuevaPosicion] != -1 && arreglo[nuevaPosicion] != num){ // Revisa si la nueva posición esta vacia
            i = i + 1; //Aumenta en uno para avanzar una posición
            nuevaPosicion = (posicion + (i*i)); // Genera una nueva posición

            if(nuevaPosicion > tamano){ 
                i = 1;
                nuevaPosicion = 0; // Vuelve al inicio del arreglo
                posicion = 0;
            }
        }

        if(arreglo[nuevaPosicion] == -1){
            cout << num << " no se encuentra en el arreglo" << endl;
            imprimirColision(arreglo, tamano);
        }

	    else{
            cout << num << " se encuentra en la posición " << nuevaPosicion << endl;
            imprimirColision(arreglo, tamano);
        }
    }
}


void ColisionesBusqueda::colisionesDobleDireccion(int posicion, int *arreglo, int tamano, int num){
    int nuevaPosicion;

    if(arreglo[posicion] != -1 && arreglo[posicion] == num){ // Revisa si el número ya esta en el arreglo
        cout << num << " se encuentra en la posición " << posicion << endl;
    }

    else{
        nuevaPosicion = ((posicion + 1)%tamano - 1) + 1;
        while(nuevaPosicion <= tamano && arreglo[nuevaPosicion] != -1 && nuevaPosicion != posicion && arreglo[nuevaPosicion] != num){
            nuevaPosicion = ((nuevaPosicion + 1)%tamano -1) + 1; // Se resta uno al tamaño porque el arreglo parte desde cero
        }

        if(arreglo[nuevaPosicion] == -1 || (nuevaPosicion == posicion)){ 
            cout << num << " no se encuentra en el arreglo" << endl;
            imprimirColision(arreglo, tamano);
        }

	    else{
            cout << num << " se encuentra en la posición " << nuevaPosicion << endl;
            imprimirColision(arreglo, tamano);
        }
    }
}

