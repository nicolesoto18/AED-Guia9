#include <iostream>
#include <string.h> // Libreria para poder utilizar strcmp
#include "ColisionesIngreso.h"
#include "ColisionesBusqueda.h" // Definición de la clase
#include "Encadenamiento.h"

using namespace std;

void imprimir(int *arreglo, int tamano){
    for(int i = 0; i < tamano; i++){
        cout << i  << " [" << arreglo[i] << "]"<< endl;
    }
}


void arregloVacio(int *arreglo, int tamano){
    for (int i = 0; i < tamano; i++){ 
        arreglo[i] = -1; // Llena el arreglo con -1 para indicar que esta vacio
    }
}


void metodoColisionesInsercion(int *arreglo, int tamano, int num, char *metodo, int posicion){ // Métodos para las colisiones en la inserción
    ColisionesIngreso colision = ColisionesIngreso();

    if(strcmp(metodo, "L") == 0){ // strcmp permite comparar un puntero, cero significa que son iguales
        colision.colisionesPruebaLineal(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "C") == 0){
        colision.colisionesPruebaCuadratica(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "D") == 0){
        colision.colisionesDobleDireccion(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "E") == 0 ){
        Encadenamiento encadenamiento = Encadenamiento();
        encadenamiento.metodoEncadenamiento(arreglo, tamano, num, posicion);
    }

    else{
        system("clear");
        cout << "\nFATAL: Error al ingresar parámetro para resolver colisiones" << endl;
        cout << "Reinicie el programa " << endl;
        cout << "Recuerde que puede ingresar: " << endl;
        cout << " [L] Prueba Líneal\n [C] Prueba cuadrática\n [D] Doble dirección hash\n [E] Encadenamiento\n" << endl;
    }    
}


void metodoColisionesBusqueda(int *arreglo, int tamano, int num, char *metodo, int posicion){ // Métodos para las colisiones en la busqueda 
    ColisionesBusqueda colision = ColisionesBusqueda();
    
    if(strcmp(metodo, "L") == 0){ // strcmp permite comparar un puntero, cero significa que son iguales
        colision.colisionesPruebaLineal(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "C") == 0){
        colision.colisionesPruebaCuadratica(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "D") == 0){
        colision.colisionesDobleDireccion(posicion, arreglo, tamano, num);
    }

    else if(strcmp(metodo, "E") == 0 ){
        Encadenamiento encadenamiento = Encadenamiento();
        encadenamiento.metodoEncadenamiento(arreglo, tamano, num, posicion);
    }

    else{
        system("clear");
        cout << "\nFATAL: Error al ingresar parámetro para resolver colisiones" << endl;
        cout << "Reinicie el programa " << endl;
        cout << "Recuerde que puede ingresar: " << endl;
        cout << " [L] Prueba Líneal\n [C] Prueba cuadrática\n [D] Doble dirección hash\n [E] Encadenamiento\n" << endl;
    }   
}


void funcionHash(int *arreglo, int tamano, int num, char *metodo){ // Determinara la posición donde se agregara el dato
    int posicion;
    
    posicion = (num%(tamano - 1)) + 1; // Función hash por módulo, asigna una posición para guardar el dato
    
    //system("clear");

    if(arreglo[posicion] != -1){ // Hay una colisión
        cout << "Colision en posicion " << posicion << endl;
        metodoColisionesInsercion(arreglo, tamano, num, metodo, posicion); // Se revisa con que método se resolver la colisión
    }

    else{
        arreglo[posicion] = num;
        imprimir(arreglo, tamano);
    }
}
    

bool arregloLleno(int *arreglo, int tamano){
    int contador;

    for(int i = 0; i < tamano; i++){
        if(arreglo[i] != -1){
            contador++;
        }
    }

    if(contador <= tamano && contador > 0){ // Hay elementos
        return true;
    }

    else{ // No hay elementos en el arreglo
        return false;
    }
}

int pedirNumero(){
    int num;

    system("clear");
    cout << "Ingrese el número" << endl;
    cin >> num; // Guardar

    return num;
}


int menu(char *metodo, int tamano){
    int num;
    int opcion;
    int posicion;
    int arreglo[tamano];
    bool banderaArreglo; // Identificara si el arreglo tiene elementos para buscar
    arregloVacio(arreglo, tamano); // Inialiazación del arreglo
   
    while(opcion != 3){
        cout << "\nIngrese una opción:" << endl; 
        cout << " [1] Ingresar números" << endl;
        cout << " [2] Buscar un número" << endl;
        cout << " [3] Salir" << endl;

        cin >> opcion;

        switch(opcion){
            case 1:
                num = pedirNumero();
                funcionHash(arreglo, tamano, num, metodo); // Buscar una posicion para agregar el número
                break;

            case 2:
                banderaArreglo = arregloLleno(arreglo, tamano); // Revisa si hay elementos 
                if(banderaArreglo == true){
                    num = pedirNumero(); // Se pide en número a buscar
                    
                    posicion = (num%(tamano - 1)) + 1; // Función hash por módulo, se define la posición donde se buscara el dato

                    system("clear");

                    if(arreglo[posicion] == num){
                        cout << num << " se encuentra en la posicion " << posicion << endl;
                        imprimir(arreglo, tamano);
                    }
   
                    else{
                        cout << "Colision en posicion " << posicion << endl;
                        metodoColisionesBusqueda(arreglo, tamano, num, metodo, posicion); // Se revisa con que método se resolver la colisión
                    }
                }

                else{
                    system("clear");
                    cout << "El arreglo esta vacio" << endl;
                }

                break;

            case 3:
                break;
            
            default:
                system("clear");
                cout << "FATAL: Opción incorrecta, intente de nuevo" << endl;
                break;
        }
    }
}


int validarNumeroParametros(int argc, char *argv[], int tamano ){
    if(argc == 2){ // Validar la cantidad de parámetros
        menu(argv[1], tamano); // argv[1] corresponde al parámetro que decidira el método para resolver las colisones
    }
    
    else{
        cout << "FATAL: No ingreso la cantidad correcta de parámetros\nReinicie el programa" << endl;
        return -1;
    }
}


int main(int argc, char *argv[]){
    int parametro;
    int tamano;

    cout << "\n\tMÉTODOS DE BUSQUEDA" << endl;
    cout << "     -------------------------" << endl; 

    cout << "Defina el tamaño del arreglo: ";
    cin >> tamano;
    
    system("clear");
    parametro = validarNumeroParametros(argc, argv, tamano);
}
